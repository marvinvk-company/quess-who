from conf.settings.base import *  # noqa

DEBUG = True
TEMPLATE_DEBUG = DEBUG

DATABASES['default']['ENGINE'] = 'django.db.backends.sqlite3'
DATABASES['default']['NAME'] = 'quess-who_development'
DATABASES['default']['PASSWORD'] = 'foobar'

CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    }
}
INSTALLED_APPS += (
    'django_extensions',
)

INTERNAL_IPS = ('127.0.0.1',)


DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS': False,
    'SHOW_TEMPLATE_CONTEXT': True,
}