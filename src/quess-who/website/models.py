from django.db import models
from django.contrib.auth.models import User

# class Test(models.Model):
#     name = models.CharField(max_length=100)
#
#     def __unicode__(self):
#         return self.name
#
#
class MyUser(models.Model):
    user = models.OneToOneField(User)
    language = models.CharField(max_length=12, default='EN')
    avatar = models.ImageField(null=True)

    def __unicode__(self):
        return self.user.username

class Card(models.Model):
    image = models.ImageField()
    answer = models.CharField(max_length=100, null=True, blank=True)

    def __unicode__(self):
        return self.answer