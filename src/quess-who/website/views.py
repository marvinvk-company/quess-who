import datetime
import random
import logging
import uuid
import Queue
import time
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, View
from models import *
from django.conf import settings
from django.contrib.auth import logout
from braces.views import JsonRequestResponseMixin, CsrfExemptMixin, LoginRequiredMixin


logger = logging.getLogger(__name__)

from pusher import Pusher
pusher = Pusher(
  app_id='128249',
  key='ec45149df2696a8d0d87',
  secret='8077df6def0fb8273f13',
  ssl=False,
  port=80
)

queue = Queue.Queue()
queue_VN = Queue.Queue()
queue_NL = Queue.Queue()
queue_EN = Queue.Queue()

class AuthTemplateView(TemplateView):

    @method_decorator(login_required(login_url='/'))
    def dispatch(self, *args, **kwargs):
        return super(TemplateView, self).dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AuthTemplateView, self).get_context_data(**kwargs)
        return context

class IndexView(TemplateView):

    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        return context


class SigninView(TemplateView):

    template_name = 'login.html'

    def get_context_data(self, **kwargs):
        context = super(SigninView, self).get_context_data(**kwargs)
        return context


class LanguageView(AuthTemplateView):

    template_name = 'language.html'

    # def get_context_data(self, **kwargs):
    #     context = super(ProfileView, self).get_context_data(**kwargs)
    #     user = self.request.user
    #     # for front end dev
    #     if user:
    #         fygUser, _ = FygUser.objects.get_or_create(user=user)
    #         fygUser.complete_profile = True
    #         fygUser.save()
    #
    #     if self.request.method == "POST":
    #         logging.info('POST: %s' % self.request.POST)
    #
    #     return context


class OpponentView(AuthTemplateView):

    template_name = 'opponent.html'

    def get_context_data(self, **kwargs):
        context = super(OpponentView, self).get_context_data(**kwargs)
        context['user_uuid'] = str(uuid.uuid4())
        return context

class PlayView(AuthTemplateView):

    template_name = 'game.html'

    def get_context_data(self, **kwargs):
        context = super(PlayView, self).get_context_data(**kwargs)
        context['user_uuid'] = self.request.GET.get('user')
        context['opponent_uuid'] = self.request.GET.get('opponent')
        context['start'] = self.request.GET.get('start')
        return context


class LeaderBoardView(AuthTemplateView):

    template_name = 'learderboard.html'

    def get_context_data(self, **kwargs):
        context = super(LeaderBoardView, self).get_context_data(**kwargs)
        return context


# class CompleteProfileView(View):
#
#     @method_decorator(login_required(login_url='/'))
#     def post(self, *args, **kwargs):
#         logging.info('FILE: %s' % self.request.FILES)
#         name = self.request.POST.get('name', '')
#         file = self.request.FILES['avatar']
#         user = self.request.user
#         fygUser = FygUser.objects.get(user=user)
#         if user:
#             user.first_name = name
#             user.save()
#         if file:
#             file_name = '/avatar_%s_%s' % (user.pk, file.name)
#             file_path = settings.MEDIA_ROOT + file_name
#             with open(file_path, 'w+') as destination:
#                 for chunk in file.chunks():
#                     destination.write(chunk)
#             fygUser.avatar = settings.MEDIA_URL + file_name
#             fygUser.save()
#
#         return HttpResponseRedirect('/complete-your-profile/?message=thanks')



class APIView(CsrfExemptMixin, JsonRequestResponseMixin, TemplateView):

    def next_question(self):
        opponent_uuid = self.request.GET.get('opponent_uuid')
        begin = self.request.GET.get('begin', '')
        if begin != '':
            begin = True
        max_id = Card.objects.all().order_by('-id')[0].pk

        all_cards = Card.objects.all().values_list('pk', flat=True)
        card1 = Card.objects.get(pk=all_cards[random.randint(0, len(all_cards) - 1)])
        card2 = Card.objects.get(pk=all_cards[random.randint(0, len(all_cards) - 1)])

        logging.info('Sent to pusher: %s' % opponent_uuid)
        pusher.trigger('game_channel', opponent_uuid, {'image': card1.image.url, 'answer': card2.answer, 'begin': begin})
        return {'image': card2.image.url, 'answer': card1.answer}

    def find_opponent(self):
        if self.request.user.is_authenticated:
            profile, _ = MyUser.objects.get_or_create(user=self.request.user)
            language = profile.language
            if language == 'EN':
                queue = queue_EN
            elif language == 'NL':
                queue = queue_NL
            elif language == 'VN':
                queue = queue_VN
            # find waiting user in the queue
            user_uuid = self.request.POST.get('user_uuid')
            opponent_uuid = None
            logging.info('queue size: %s' % queue.qsize())
            while not opponent_uuid and not queue.empty():
                opponent_obj = queue.get()
                logging.info('opponent_obj: %s' % opponent_obj)
                if time.time() - opponent_obj['timestamp'] > 20:
                    opponent_uuid = None
                else:
                    opponent_uuid = opponent_obj['user_uuid']

            if not opponent_uuid or opponent_uuid == user_uuid:
                queue.put({'user_uuid': user_uuid, 'timestamp': time.time()})
            else:
                room_number = str(uuid.uuid4())
                pusher.trigger('player_channel', opponent_uuid, {'room': room_number, 'start': 1,
                                                                'opponent_uuid': user_uuid,
                                                                 'user_uuid': opponent_uuid})
                pusher.trigger('player_channel', user_uuid, {'room': room_number, 'start': 0,
                                                             'opponent_uuid': opponent_uuid,
                                                             'user_uuid': user_uuid})

    def post(self, request, *args, **kwargs):
        data = {}
        func_name = kwargs.get('func_name')
        logger.info('opponent_obj: %s' % func_name)
        if func_name == 'findopponent':
            self.find_opponent()
        elif func_name == 'sendhint':
            opponent_uuid = self.request.POST.get('opponent_uuid')
            message = self.request.POST.get('message')
            correct_guessing = self.request.POST.get('correct_guessing')
            pusher.trigger('hint_channel', opponent_uuid, {
                'message': message, 'correct_guessing': correct_guessing})
        elif func_name == 'login':
            email = self.request.POST.get('email', '')
            password = self.request.POST.get('password', '')
            user = authenticate(username=email, password=password)
            if user:
                login(self.request, user)
                return HttpResponseRedirect('/opponent/')
            else:
                return HttpResponseRedirect('/?e=wrong-authen')

        elif func_name == 'signup':
            name = self.request.POST.get('email', '')
            email = self.request.POST.get('email', '')
            password = self.request.POST.get('password', '')
            #check email
            user = User.objects.filter(email='email')
            if user:
                return HttpResponseRedirect('/?e=duplicate_email')
            else:
                user = User.objects.create_user(email, email, password)
                user.first_name = name
                user.save()
                user = authenticate(username=email, password=password)
                login(self.request, user)
                return HttpResponseRedirect('/opponent/')

        elif func_name =='signout':
            logout(self.request)
            return HttpResponseRedirect('/')
        elif func_name == 'profile':
            language = self.request.POST.get('language')
            myuser, _ = MyUser.objects.get_or_create(user=self.request.user)
            myuser.language = language
            myuser.save()
            return HttpResponseRedirect('/opponent/')

        return self.render_json_response(data)

    def get(self, request, *args, **kwargs):
        data = {}
        func_name = kwargs.get('func_name')
        if func_name =='signout':
            logout(self.request)
            return HttpResponseRedirect('/')
        elif func_name == 'nextquestion':
            data = self.next_question()
        return self.render_json_response(data)