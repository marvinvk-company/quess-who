from django import template

register = template.Library()

@register.filter(name='mod')
def mod(value, arg):
    return int(value) % arg

@register.filter(name='divide')
def divide(value, arg):
    return int(value) / arg