import decimal
import datetime
import simplejson
import locale
class PrettyFloat(float):
    def __repr__(self):
        return '%.15g' % self

class NumberUtils(object):
    @staticmethod
    def __format_currency_inner(val, decimal_places=0):
        return "$" + locale.format("%0.{0}f".format(decimal_places), val, grouping=True)
    
    @staticmethod
    def format_currency(val, decimal_places=2):
        try:
            if val == None:
                return None;
            
            if val < 0:
                txt = '(%s)' % NumberUtils.__format_currency_inner(-val, decimal_places);
            else:
                txt = NumberUtils.__format_currency_inner(val, decimal_places);
            return txt;
        except Exception:
            return val
        
    @staticmethod
    def format_number(val, decimal_places = 0):
        try:
            if val == None:
                return None;
            
            if decimal_places > 0:
                txt = locale.format("%0.{0}f".format(decimal_places), val, grouping=True)
            else:
                txt = locale.format("%d", val, grouping=True)
            return txt
        except Exception:
            return val;
    
    @staticmethod
    def format_percentage(decimal, decimal_places = 2):
        format_str = '0.{0}%'.format(decimal_places)
        return format(decimal, format_str);
    
    @staticmethod
    def get_safe_value(func, obj, default='#N/A'):
        try:
            return func(obj)
        except Exception:
            return default


class EncodingUtils(object):
    @staticmethod
    def encode_json(val, default=None):
        if default:
            return simplejson.dumps(val, default=default, sort_keys=True)
        return simplejson.dumps(val, default=EncodingUtils.__json_default_encode, sort_keys=True)
    
    @staticmethod
    def __json_default_encode(obj):
        if isinstance(obj, datetime.datetime):
            return obj.isoformat()
        if isinstance(obj, decimal.Decimal):
            #return NumberUtils.format_currency(obj)
            return PrettyFloat(str(obj))
        
        return str(obj)
    
    @staticmethod
    def json_gui_encode(obj):
        if isinstance(obj, datetime.datetime):
            return obj.strftime('%m/%d/%Y %H:%M:%S')
        if isinstance(obj, decimal.Decimal):
            return NumberUtils.format_currency(obj)
        
        return str(obj)