from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from .views import IndexView, OpponentView, PlayView, \
    LeaderBoardView, LanguageView, \
    SigninView, APIView


admin.autodiscover()


urlpatterns = patterns(
    '',
    url(r'^$', IndexView.as_view()),
    url(r'^login/$', SigninView.as_view()),
    url(r'^language/$', LanguageView.as_view()),
    url(r'^opponent/$', OpponentView.as_view()),
    url(r'^game/$', PlayView.as_view()),
    url(r'^leader-board/$', LeaderBoardView.as_view()),

    url(r'api/(?P<func_name>\w+)/$', APIView.as_view()),

    # Django admin.
    url(r'^admin/', include(admin.site.urls)),
)

# Route for media files in local development.
if settings.DEBUG:
    # This serves static files and media files.
    urlpatterns += staticfiles_urlpatterns()
    # In case media is not served correctly
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
            }),
    )
