import StringIO
import csv
import logging

from django import http
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template.context import RequestContext, Context
from django.template.loader import get_template
from django.utils.decorators import method_decorator
# from django.utils.functional import update_wrapper
from functools import WRAPPER_ASSIGNMENTS, update_wrapper, wraps
from website.utils import EncodingUtils


logger = logging.getLogger(__name__)
class View(object):
    """
    Intentionally simple parent class for all views. Only implements
    dispatch-by-method and simple sanity checking.
    """

    http_method_names = ['get', 'post', 'put', 'delete', 'head', 'options', 'trace']

    def __init__(self, **kwargs):
        """
        Constructor. Called in the URLconf; can contain helpful extra
        keyword arguments, and other things.
        """
        # Go through keyword arguments, and either save their values to our
        # instance, or raise an error.
        for key, value in kwargs.iteritems():
            setattr(self, key, value)

    @classmethod
    def as_view(cls, **initkwargs):
        """
        Main entry point for a request-response process.
        """
        # sanitize keyword arguments
        for key in initkwargs:
            if key in cls.http_method_names:
                raise TypeError(u"You tried to pass in the %s method name as a "
                                u"keyword argument to %s(). Don't do that."
                                % (key, cls.__name__))
            if not hasattr(cls, key):
                raise TypeError(u"%s() received an invalid keyword %r" % (
                    cls.__name__, key))

        def view(request, *args, **kwargs):
            self = cls(**initkwargs)
            return self.dispatch(request, *args, **kwargs)

        # take name and docstring from class
        update_wrapper(view, cls, updated=())

        # and possible attributes set by decorators
        # like csrf_exempt from dispatch
        update_wrapper(view, cls.dispatch, assigned=())
        return view

    def dispatch(self, request, *args, **kwargs):
        # Try to dispatch to the right method; if a method doesn't exist,
        # defer to the error handler. Also defer to the error handler if the
        # request method isn't on the approved list.
        if request.method.lower() in self.http_method_names:
            handler = getattr(self, request.method.lower(), self.http_method_not_allowed)
        else:
            handler = self.http_method_not_allowed
        self.request = request
        self.args = args
        self.kwargs = kwargs
        return handler(request, *args, **kwargs)

    def http_method_not_allowed(self, request, *args, **kwargs):
        allowed_methods = [m for m in self.http_method_names if hasattr(self, m)]
        logger.warning('Method Not Allowed (%s): %s' % (request.method, request.path),
            extra={
                'status_code': 405,
                'request': self.request
            }
        )
        return http.HttpResponseNotAllowed(allowed_methods)

    def head(self, request, *args, **kwargs): 
        return self.get(request, *args, **kwargs) 

class AuthenticatedView(View):
    '''
    A view that need to authenticate and check permission.
    '''
    permissions = []
    
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(AuthenticatedView, self).dispatch(request, *args, **kwargs)
    
    def get_context_data(self, **kwargs):
        return {
            'params': kwargs
        }
        
    def get(self, request, *args, **kwargs):
        'Default HTTP Method of View'
        return self.do_work(request, self.do_get, *args, **kwargs)
    
    def post(self, request, *args, **kwargs):
        return self.do_work(request, self.do_post, *args, **kwargs)
    
    def do_get(self, request, *args, **kwargs):
        return {}
    
    def do_post(self, request, *args, **kwargs):
        return {}
    
    def do_work(self, request, httpmethod_func, *args, **kwargs):
        for perm in self.permissions:
            if not request.user.has_perm(perm):
                response = http.HttpResponseForbidden()
                break
        else:
            self.context = self.get_context_data(**kwargs)
            self.data = httpmethod_func(request, *args, **kwargs)
            if isinstance(self.data, http.HttpResponse):
                response = self.data
            else:
                self.context['data'] = {'Code': 0,
                                        'HasError': False,
                                        'Message':''}
                if type(self.data) is not dict:
                    self.context['data'] = self.data
                else:
                    self.context['data'].update(self.data)
                    #for legacy compatible
                    self.context.update(self.data)
                self.context['method'] = request.method.lower()
                response = self.render_to_response(self.context)
        
        return response

class JsonErrorResponseMixin(object):
    '''
    Used to render JSON data when AJAX request has error.
    '''
    data = {}
    def render_to_response(self, context, **response_kwargs):
        "Returns a JSON response"
        return self.get_json_response(EncodingUtils.encode_json(self.data))
    
class JsonResponseMixin(object):
    '''
    Used to render JSON response.
    '''
    data = {}
    def render_to_response(self, context, **response_kwargs):
        "Returns a JSON response"
        return self.get_json_response(self.get_json_data(context), **response_kwargs)
    
    def get_json_response(self, content, **httpresponse_kwargs):
        "Construct an `HttpResponse` object."
        return HttpResponse(content,
                            content_type='application/json; charset=utf-8',
                            **httpresponse_kwargs)
        
    def get_json_data(self, context):
        "Convert the context dictionary into a JSON object"
        return EncodingUtils.encode_json(self.get_data(context))

    def get_data(self, context={}):
        data = context.get('data')
        if data:
            self.data = data 
        return self.data
    
class AjaxView(JsonResponseMixin, JsonErrorResponseMixin, AuthenticatedView):
    '''
    A view is used to work with AJAX and returns JSON response.
    '''
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return super(AuthenticatedView, self).dispatch(request, *args, **kwargs)
        else:
            self.data = {'ErrorCode': -403,
                         'HasError': True,
                         'Message': 'Session is out of date.'}
            
            context = AuthenticatedView.get_context_data(self)
            response = JsonErrorResponseMixin.render_to_response(self, context)
            return response 
    
    def do_work(self, request, httpmethod_func, *args, **kwargs):
        try:
            response = AuthenticatedView.do_work(self, request, httpmethod_func, *args, **kwargs)
        except Exception, ex:
            logger.error('AjaxView Exception="%s" Data=%s' % (str(ex), str(self.data)[:255]))
            logger.exception(ex)
            #Overwrite data to get Error Response
            self.data = {'ErrorCode': -1,
                         'HasError': True,
                         'Message': str(ex)}
            
            context = AuthenticatedView.get_context_data(self)
            f = self.request.GET.get('f','json').lower()
            if f == 'csv':
                response = self.render_to_response(context)
            else:
                response = JsonErrorResponseMixin.render_to_response(self, context)
            
        return response
    
    
class AjaxHandleView(AjaxView):
    handlers = {}
    
    def do_get(self, request, **kwargs):
        return self.__do_inner(request, **kwargs);
    
    def do_post(self, request, **kwargs):
        return self.__do_inner(request, **kwargs);
    
    def get_handlers(self):
        return self.handlers
    
    def __do_inner(self, request, **kwargs):
        handlers = self.get_handlers()
        action = kwargs.get('action')
        if action:
            handler = handlers.get(action)
            if handler:
                return handler(request)