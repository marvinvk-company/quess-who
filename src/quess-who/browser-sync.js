module.exports = {
    debugInfo: true,
    files: [
        'static/css/*.css',
        'static/**/*.scss',
        'templates/*.html'
    ],
    ghostMode: {
        forms: true,
        links: true,
        scroll: true
    },
    server: {
        baseDir: 'html'
    }
};