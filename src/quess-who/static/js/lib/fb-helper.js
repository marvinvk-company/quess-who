/*
 |--------------------------------------------------------
 |	Router
 |--------------------------------------------------------
 */
define(['facebookSDK', 'jquery'], function(FB, $) {

	return function(options) {
		// default app settings
		var defaults = {
			cookie : true,
			version: 'v2.2',
			status : true
		};

		// error codes
		var LOGIN_CANCEL = 101;

		// extend options with default settings
		options = $.extend({}, options, defaults);

		// call Facebook Init
		FB.init(options);

		// handle Facebook API response
		var fbCallback = function(deferred) {
			return function(response) {
				// reject deffered object if there's no response
				if (!response) {
					return deferred.reject('Facebook API call did not succeed.');
				}

				// reject deffered object if there's an error
				if (response.error) {
					return deferred.reject(response.error);
				}

				// resolve deffered object
				deferred.resolve(response);
			};
		};

		var authResponse = {},
			permissions = null;

		return {

			getAccessToken: function() {
				return authResponse.accessToken;
			},

			login: function (opts) {
				var self     = this,
					options  = {},
					deferred = $.Deferred();

				// parse options
				if (typeof opts === 'string') {
					options.scope = opts;
				} else {
					options = opts;
				}

				FB.login(function(response) {
					if (response.authResponse) {
						// store auth response
						authResponse = response.authResponse;

						// get and cache permissions
						self.permissions();

						// get user's data
						FB.api('/me', fbCallback(deferred));
					} else {
						deferred.reject('User cancelled login.', LOGIN_CANCEL);
					}
				}, options);

				// return promise
				return deferred.promise();
			},

			status: function() {
				var deferred = $.Deferred();

				// get login status
				FB.getLoginStatus(function(response) {
					if (response.authResponse) {
						// store auth response
						authResponse = response.authResponse;
					}

					deferred.resolve(response);
				});

				// return promise
				return deferred.promise();
			},

			me: function(options) {
				if (!options) {
					options = {};
				}

				var deferred = $.Deferred();

				this.status().then(function(response) {

					if (response.status === 'not_authorized') {
						return deferred.reject(response.status);
					}

					// store auth response
					authResponse = response.authResponse;

					// get user's data
					FB.api('/me', options, fbCallback(deferred));
				});

				// return promise
				return deferred.promise();
			},

			deletePermissions: function() {
				var deferred = $.Deferred();

				// delete permissions
				FB.api('/me/permissions', 'DELETE', fbCallback(deferred));

				// return promise
				return deferred.promise();
			},

			share: function (options) {
				var deferred = $.Deferred();

				FB.ui(options || {}, fbCallback(deferred));

				// return promise
				return deferred.promise();
			},

			albums: function(options) {
				var deferred = $.Deferred();

				options = _.extend({}, options, {accessToken: authResponse.accessToken});

				FB.api('/me/albums', options, fbCallback(deferred));

				// return promise
				return deferred.promise();
			},

			photos: function(albumId, options) {
				var deferred = $.Deferred();

				FB.api('/' + albumId + '/photos', options, fbCallback(deferred));

				// return promise
				return deferred.promise();
			},

			permissions: function() {
				var deferred = $.Deferred();

				FB.api('/me/permissions', fbCallback(deferred));

				deferred.then(function(data) {
					// cache permissions
					permissions = data.data;
				});

				// return promise
				return deferred.promise();
			},

			hasPermission: function(permission) {
				var deferred = $.Deferred();

				var hasPermissionsSuccess = function(data) {
					var permissions = data.data,
						result = false,
						p, i = 0;

					for (i; i< permissions.length; i++) {
						p = permissions[i];

						if (p.permission === permission && p.status === 'granted') {
							// if the user granted for the requested permission
							// then mark result as true
							// and break the for loop
							result = true;
							break;
						}
					}

					// resolve promise
					deferred.resolve(result);
				};

				var hasPermissionsError = function(error) {
					// reject promise on error
					deferred.reject(error);
				};

				if (!permissions) {
					// get permissions from FB Graph API
					this.permissions().then(hasPermissionsSuccess, hasPermissionsError);
				} else {
					// check existing permissions
					hasPermissionsSuccess({data: permissions});
				}

				// return promise
				return deferred.promise();
			}

		};
	};

});
