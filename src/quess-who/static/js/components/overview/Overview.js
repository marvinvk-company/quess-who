define(function(require) {

	var React = require('react'),
		Backbone = require('backbone'),
		Sidebar = require('jsx!./sidebar/Sidebar'),
		Entertainers = require('jsx!./Entertainers'),
		Paginator = require('jsx!./Paginator'),
		EntertainersCollection = require('collections/entertainers');

	return React.createClass({

		componentWillMount: function() {

			this.state.collection.on('sync', function(collection) {
				this.setState({
					filter: collection.queryParams,
					collection: collection
				});
			}.bind(this));

			this.state.collection.fetch(this.state.filter);
		},

		getInitialState: function() {
			return {
				filter: {
					q: "",
					category: "all",
					gender: "both",
					age: {
						min: 18,
						max: 80
					},
					price: {
						min: 10,
						max: 300
					}
				},
				collection: EntertainersCollection
			};
		},

		/**
		 * Filter the collection
		 * @param key
		 * @param val
		 */
		filterCollection: function(key, val) {
			var new_value = {};
			new_value[key] = val;

			var new_state = React.addons.update(this.state, {
				filter: {$merge: new_value}
			});

			// fetch collection with filter
			this.state.collection.fetch(new_state.filter);
		},

		update: function() {
			this.forceUpdate();
		},

		render: function() {

			return (
				<div className="overview">
					<Sidebar
						collection={this.state.collection}
						filter={this.state.filter}
						onChange={this.filterCollection}
						onPaginate={this.update}
					/>
					<Entertainers
						collection={this.state.collection}
					/>
					<Paginator
						collection={this.state.collection}
						onPaginate={this.update}
					/>
				</div>
			);
		}
	});
});