define(function(require) {

	var React = require('react');

	return React.createClass({

		paginateCollection: function(e) {
			e.preventDefault();

			var className = e.target.className,
				current = this.props.collection.state.currentPage,
				last = this.props.collection.state.lastPage;

			switch(true) {
				case className.indexOf("prev") > -1:

					// get last page
					if(current == 1) {
						this.props.collection.getLastPage();
						break;
					}

					// get previous page
					this.props.collection.getPreviousPage();
					break;
				default:

					// get first page
					if(current == last) {
						this.props.collection.getFirstPage();
						break;
					}
					this.props.collection.getNextPage();
			}

			this.props.onPaginate();
		},

		render: function() {
			var total = this.props.collection.state.totalRecords,
				current = this.props.collection.state.currentPage,
				pages = this.props.collection.state.totalPages;

			return (
				<div className="pagination">
					<div className="wrapper">
						<a href="#"
							className="button purple prev"
							name="page"
							value="down"
							onClick={this.paginateCollection}>
						{'<<'}
						</a>
						<div className="numbers">
							<h4>
								{total} entertainers • page {current}/{pages}
							</h4>
						</div>
						<a href="#"
							className="button purple next"
							name="page"
							value="up"
							onClick={this.paginateCollection}>
						{'>>'}
						</a>
					</div>
				</div>
			);
		}
	});
});