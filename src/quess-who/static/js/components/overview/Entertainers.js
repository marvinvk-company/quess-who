define(function(require) {

	var React = require('react');

	return React.createClass({

		render: function() {

			// entertainer component
			var entertainers = this.props.collection.map(function(entertainer, index) {
				return (
					<li
						data-gender={entertainer.get('gender')}
						data-age={entertainer.get('age')}
						data-price={entertainer.get('age')}
						key={index}
					>
						<button href="#" className="button purple add-popup">+</button>

						<a href="profile-route">
							<div>
								<img className="image" src={entertainer.get('photo_medium')}/>
								<h4>&euro;{entertainer.get('price')},-</h4>
								<h3>{entertainer.get('stagename')}</h3>
							</div>
						</a>
					</li>
				);
			}.bind(this));

			return (
				<ul className="dancers search-in">
					{entertainers}
				</ul>
			);
		}
	});
});