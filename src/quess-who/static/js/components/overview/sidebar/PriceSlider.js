define(function(require) {

	var React = require('react');

	return React.createClass({

		componentWillMount: function() {
		},

		componentDidMount: function() {
			$('.price-range').slider({
				range: true,
				min: 10,
				max: 300,
				values: [this.props.min, this.props.max],
				// filter collection on release
				change: function(e, ui) {
					this.props.onChange('price',{
						min: ui.values[0],
						max: ui.values[1]
					});
				}.bind(this),
				// update value on slide
				slide: function(e, ui) {
					this.setState({
						min: ui.values[0],
						max: ui.values[1]
					});
				}.bind(this)
			});
		},

		getInitialState: function() {
			return {
				min: this.props.min,
				max: this.props.max,
			};
		},

		render: function() {

			return (
				<div className="row">
					<h3>Price <span className="small">(per hour)</span></h3>
					<div className="slider-range-holder price-slider">
						<span className="from">
							{this.state.min}
						</span>
						<div className="slider-range price-range"></div>
						<span className="to">
							{this.state.max}
						</span>
					</div>
				</div>
			);
		}
	});
});