define(function(require) {

	var React = require('react'),
		CategoryFilter = require('jsx!./CategoryFilter'),
		GenderFilter = require('jsx!./GenderFilter'),
		AgeSlider = require('jsx!./AgeSlider'),
		PriceSlider = require('jsx!./PriceSlider'),
		AmountSelector = require('jsx!./AmountSelector');

	return React.createClass({

		paginateCollection: function(e) {
			e.preventDefault();

			this.props.collection.setPageSize(
				parseInt(e.target.value)
			).getFirstPage();

			this.props.onPaginate();
		},

		submit: function(e) {
			e.preventDefault();
			var val = React.findDOMNode(this.refs.search).value;

			return this.props.onChange('q', val);
		},

		render: function() {

			return (
				<form className="filter-bar clearfix" onSubmit={this.submit}>
					<input ref="search" type="text" className="search" name="search" placeholder="search..."/>
					<CategoryFilter
						defaultValue={this.props.filter.category}
						onChange={this.props.onChange}
						categories={this.props.collection.state.categories}
					/>
					<GenderFilter
						defaultValue={this.props.filter.gender}
						onChange={this.props.onChange}
					/>
					<AgeSlider
						min={this.props.filter.age.min}
						max={this.props.filter.age.max}
						onChange={this.props.onChange}
					/>
					<PriceSlider
						min={this.props.filter.price.min}
						max={this.props.filter.price.max}
						onChange={this.props.onChange}
					/>
					<AmountSelector
						value={this.props.max}
						onChange={this.paginateCollection}
					/>
				</form>
			);
		}
	});
});