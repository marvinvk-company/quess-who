define(function(require) {

	var React = require('react');

	return React.createClass({

		render: function() {
			var options = [20, 40, 60];

			// option components
			var dropdownOptions = options.map(function(option, index) {
				return <option value={option} key={index}>
					{option}
				</option>
			}.bind(this));

			return (
				<div className="row">
					<h3>Per page </h3>
					<div className="col-24">
						<select
							id="perPage"
							className="field-holder__input field-holder__input--select"
							name="max"
							defaultValue={this.props.value}
							onChange={this.props.onChange}>
						{dropdownOptions}
						</select>
					</div>
				</div>
			);
		}
	});
});