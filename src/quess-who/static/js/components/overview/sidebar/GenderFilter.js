define(function(require) {

	var React = require('react');

	return React.createClass({

		onChange: function(e) {
			this.props.onChange('gender', e.target.value);
		},

		render: function() {
			var options = ["both", "male", "female"];

			// radio button component
			var radioButtons = options.map(function(option, index) {
				return (
					<div className="col-24" key={index}>
						<input
							id={option}
							type="radio"
							name="gender"
							value={option}
							onChange={this.onChange}
							defaultChecked={this.props.defaultValue === option}
						/>
						<label htmlFor={option} className="option">
							<span className="radio">
								<span className="check"></span>
							</span>
							{option}
						</label>
					</div>
				);
			}.bind(this));

			return (
				<div className="row">
					<h3>Gender</h3>
					<div className="radio-holder clearfix">
						{radioButtons}
					</div>
				</div>
			);
		}
	});
});