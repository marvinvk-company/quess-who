define(function(require) {

	var React   = require('react');

	return React.createClass({

		componentDidMount: function() {
			$('.age-range').slider({
				range: true,
				min: 18,
				max: 80,
				values: [this.props.min, this.props.max],
				// filter collection on release
				change: function(e, ui) {
					this.props.onChange('age',{
						min: ui.values[0],
						max: ui.values[1]
					});
				}.bind(this),
				// update value on slide
				slide: function(e, ui) {
					this.setState({
						min: ui.values[0],
						max: ui.values[1]
					});
				}.bind(this)
			});
		},

		getInitialState: function() {
			return {
				min: this.props.min,
				max: this.props.max,
			};
		},

		render: function() {

			return (
				<div className="row">
					<h3>Age</h3>
					<div className="slider-range-holder age-slider">
						<span className="from">
							{this.state.min}
						</span>
						<div className="slider-range age-range"></div>
						<span className="to">
							{this.state.max}
						</span>
					</div>
				</div>
			);
		}
	});
});