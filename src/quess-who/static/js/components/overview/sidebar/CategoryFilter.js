define(function(require) {

	var React = require('react');

	return React.createClass({

		onChange: function(e) {
			this.props.onChange('category', e.target.value);
		},

		render: function() {

			// radio button component
			var radioButtons = this.props.categories.map(function(category, index) {
				return (
					<div className="col-24" key={index}>
						<input
							id={category}
							type="radio"
							name="category"
							onChange={this.onChange}
							value={category}
							defaultChecked={this.props.defaultValue === category}
						/>
						<label htmlFor={category} className="option">
							<span className="radio">
								<span className="check"></span>
							</span>
						{category}
						</label>
					</div>
				);
			}.bind(this));

			return (
				<div className="row">
					<h3>Category</h3>
					<div className="radio-holder clearfix">
						<div className="col-24">
							<input
								id="all"
								type="radio"
								name="category"
								value="all"
								defaultChecked={this.props.defaultValue === 'all'}
							/>
							<label htmlFor="all" className="option">
								<span className="radio">
									<span className="check"></span>
								</span>
							all
							</label>
						</div>
						{radioButtons}
					</div>
				</div>
			);
		}
	});
});