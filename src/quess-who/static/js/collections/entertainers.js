define(function(require) {

	var PageableCollection = require('backbonePaginator'),
		Backbone = require('backbone');

	var Entertainer = Backbone.Model.extend({

		defaults: {
			stagename: ''
		}

	});

	var EntertainersCollection = Backbone.PageableCollection.extend({

		url: '/api/entertainers',

		mode: 'client',

		model: Entertainer,

		state: {
			pageSize: 20,
			order: 0,
			categories: []
		},

		parse: function(response) {
			var categories = [];

			// get all unique categories
			$.each(response, function(index, entertainer) {

				// entertainer has categories
				if(entertainer.categories.length > 0) {

					// add values uniquely
					entertainer.categories.map(function(category) {
						if(categories.indexOf(category.name) > -1) return false;
						categories.push(category.name);
					});
				}
			});

			this.state.categories = categories;

			// return og data
			return response;
		},

		fetch: function(filter) {

			// set filter params
			if(typeof filter != 'undefined') {
				this.queryParams = filter;
			}

			// call Backbone.PageableCollection.fetch
			Backbone.PageableCollection.prototype.fetch.call(this);
		}
	});

	return new EntertainersCollection();

});