/**
 * Login view
 */
define([
	'backbone',
	'models/user'
], function (Backbone, userModel) {

	var LoginView = Backbone.View.extend({

		el: 'login',

		events: {
			'click .social-button--facebook': 'facebookLogin'
		},

		initialize: function() {
			this.model = new userModel();
		},

		facebookLogin: function(e) {
			e.preventDefault();
			this.model.facebookLogin();
		}

	});

	return LoginView;

});
