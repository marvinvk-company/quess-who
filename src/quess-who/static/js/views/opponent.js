/**
 * Opponent view
 */
define([
	'backbone',
	'pusher'
], function (Backbone, Pusher) {

	var OpponentView = Backbone.View.extend({

		el: 'opponent',

		user_uuid: '',

		events: {
			'click .opponent__button': 'getRandomUser'
		},

		initialize: function() {
			var pusher = new Pusher('ec45149df2696a8d0d87', {
				//encrypted: true
			});
			var channel = pusher.subscribe('player_channel');
			this.user_uuid = this.$('#user_uuid').val();

			channel.bind(this.user_uuid, this.toURL);

			TweenMax.staggerFrom(this.$('.friend'), 0.7, {
				opacity: 0,
				scale: 0,
				y: -20,
				ease: 'easeOutBack'
			}, 0.15);
		},

		toURL: function(data) {
			console.log(data.room);
			window.location = '/game/?start=' + data.start +
				'&opponent=' + data.opponent_uuid +
				'&user=' + data.user_uuid +
				'#' + data.room;
		},

		getRandomUser: function(e) {
			$('.opponent__button').addClass('button--loading');
			e.preventDefault();

			$.post( "/api/findopponent/", { user_uuid: this.user_uuid }, function(data) {});
		}

	});

	return OpponentView;

});
