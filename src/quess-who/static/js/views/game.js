/**
 * Opponent view
 */
define([
	'backbone',
	'pusher',
	'webrtc'
], function (Backbone, Pusher) {

	var GameView = Backbone.View.extend({

		el: 'game',

		opponent_uuid: '',

		user_uuid: '',

		count: 0,

		answer: 'random_answer',

		events: {
			'click .game__button': 'nextQuestion',
			'keydown': 'keyAction'
		},

		initialize: function() {
			// init webrtc
			pageReady();

			// tween in card
			TweenMax.from(this.$('.game__card'), 1.2, {
				delay: 0.2,
				opacity: 0,
				y: -500,
				ease: 'easeInOutBack'
			});

			var pusher = new Pusher('ec45149df2696a8d0d87', {
				//encrypted: true
			});
			var channel = pusher.subscribe('game_channel');
			this.user_uuid = this.$('#user_uuid').val();
			this.opponent_uuid = this.$('#opponent_uuid').val();
			this.count = -1;
			var _this = this;

			channel.bind(this.user_uuid, function(data) {
				$('.game__card').attr('src', data.image);
				if (data.begin == true) {
					_this.answer = data.answer;
					console.log('tonext: ' + data.answer);
				}
			});

			$.get("/api/nextquestion/", {opponent_uuid: this.opponent_uuid, begin: true}, function (data) {
				// change image
				$('.game__card').attr('src', data.image);
				_this.answer = data.answer;
				console.log('get: ' + data.answer);
			});

			var hintChannel = pusher.subscribe('hint_channel');
			hintChannel.bind(this.user_uuid, function(data) {
				$('#friend_typing').show();
				if (data.correct_guessing == 'true') {
					$('#friend_typing').html("<div style='color:green;'>You friend guessed it correctly</div>").fadeOut(7000);
				}
				else {
					$('#friend_typing').html("You friend's guessing: " + data.message).fadeOut(7000);
				}
			});
		},

		keyAction: function(e) {
			var code = e.keyCode || e.which;

			// if enter is pressed
			if (code == 13) {
				this.nextQuestion();
			}
		},

		nextQuestion: function() {
			var _this = this;
			var correct_guessing = (this.$('.game__answer').val().trim().toLowerCase() == this.answer.trim().toLowerCase());
			$.post("/api/sendhint/", {opponent_uuid: _this.opponent_uuid,
				message: this.$('.game__answer').val(), correct_guessing: correct_guessing}, function (data) {
					//do nothing
			});
			if (correct_guessing == false) {
				$('.game__answer').addClass('game__answer--wrong');
				return;
			}
			else {
				$('.game__answer').removeClass('game__answer--wrong');
				$('.game__answer').addClass('game__answer--correct');
				$('.game__button').addClass('button--loading');

				$.get("/api/nextquestion/", {opponent_uuid: this.opponent_uuid}, function (data) {
					//$('.game__card').attr('src', data.image);
					_this.answer = data.answer;
					console.log('next: ' + data.answer);
					setTimeout(function(){
						$('.game__button').removeClass('button--loading');
						$('.game__answer').removeClass('game__answer--correct');
						$('.game__answer').val('');
					}, 5000);
				});
			}
		}

	});

	return GameView;

});
