/**
 * Home view
 */
define([
	'backbone',
	'models/user'
], function (Backbone, userModel) {

	var HomeView = Backbone.View.extend({

		el: 'home',

		events: {
			'click .social-button--facebook': 'facebookConnect'
		},

		initialize: function() {
			this.model = new userModel();
		},

		/**
		 * Facebook connect
		 */
		facebookConnect: function() {

			this.model.facebookConnect();
		}

	});

	return HomeView;

});
