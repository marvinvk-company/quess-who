/**
 * Language view
 */
define([
	'backbone'
], function (Backbone) {

	var LanguageView = Backbone.View.extend({

		el: 'language',

		initialize: function() {

		}

	});

	return LanguageView;

});
