/**
 * Main view
 */
define(function(require) {

	var Backbone = require('backbone'),
		HomeView = require('views/home'),
		LoginView = require('views/login'),
		LanguageView = require('views/language'),
		OpponentView = require('views/opponent'),
		GameView = require('views/game');

	var MainView = Backbone.View.extend({

		el: 'body',

		initialize: function() {
			// bind window events
			$(window).on('resize', _.bind(this.onWindowResize, this));
			$(window).on('scroll', _.bind(this.onWindowScroll, this));

			// trigger window events callbacks on page load
			this.onWindowResize();
			this.onWindowScroll();
		},

		onWindowResize: function() {
			App.Settings.winWidth = $(window).width();
			App.Settings.winHeight = $(window).height();
		},

		onWindowScroll: function() {
			App.Settings.scrollTop = $(window).scrollTop();
		},

		use: function(controllerName, $el) {
			var options = {
				el: $el
			};

			// init view
			var viewUsed = this[controllerName].call(this, options);

			// fade in loaded view
			TweenMax.to(viewUsed.$el, 0.8, {
				delay: 0.2,
				autoAlpha: 1
			});
		},

		homeController: function(options) {
			return new HomeView(options);
		},

		loginController: function(options) {
			return new LoginView(options);
		},

		languageController: function(options) {
			return new LanguageView(options);
		},

		opponentController: function(options) {
			return new OpponentView(options);
		},

		gameController: function(options) {
			return new GameView(options);
		}

	});

	return MainView;

});
