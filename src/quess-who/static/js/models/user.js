/*
|--------------------------------------------------------
|	User Model
|--------------------------------------------------------
*/
define(['backbone'], function(Backbone) {

	return Backbone.Model.extend({

		urlRoot: 'api/users',

		defaults: {
			status: null,
			fb_id: 0,
			name: '',
			email: '',
			thumbnail: ''
		},

		isSaving: false,

		initialize: function() {
			this.on('change:name', function(model, value) {
				this.get('order').set('name', value);
			}, this);
		},

		/**
		 * Override Backbone.Model.toJSON method
		 * @returns {object}
		 */
		toJSON: function() {
			var jsonData = Backbone.Model.prototype.toJSON.call(this);

			return jsonData;
		},

		/**
		 * Connect to Facebook
		 */
		facebookConnect: function() {
			fbHelper.login({scope: 'email'}).then(_.bind(this.checkUserStatus, this, {}));
		},

		/**
		 * On Facebook status check response
		 * @param {object} deferred
		 * @param {object} response
		 */
		onStatusCheck: function(deferred, response) {
			// set saveUser options
			var options = {
				success : _.bind(deferred.resolve, deferred),
				error   : _.bind(deferred.reject, deferred)
			};

			if (response.status === 'connected') {
				// fetch Facebook data and then save user
				fbHelper.me().then(_.bind(this.checkUserStatus, this, options));
			} else {
				// resolve promise
				deferred.resolve(this, response);

				// update user status (not_authorized)
				this.set('status', response.status);
			}
		},

		/**
		 * Check user status in backend
		 * @param {object} options
		 * @param {object} response
		 */
		checkUserStatus: function(options, response) {
			// add url to options
			options.url = this.url() + '/' + response.id + '/fb';

			// update user model
			this.set({
				fb_id: response.id,
				name: response.name,
				email: response.email,
				thumbnail: '//graph.facebook.com/' + response.id + '/picture?width=125&height=125'
			});

			// fetch/check user depending on Facebook id
			this.fetch(options);
		}

	});
});
