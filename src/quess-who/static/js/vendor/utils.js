define(['jquery'], function() {
	/**
	 * Log history
	 */
	window.log = function(){
		log.history = log.history || [];   // store logs to an array for reference
		log.history.push(arguments);
		if(this.console) {
				arguments.callee = arguments.callee.caller;
				console.log( Array.prototype.slice.call(arguments) );
		}
	};

	/**
	 * Console log, make it safe to use console.log always
	 */
	(function(b){function c(){}for(var d="assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","),a;a=d.pop();)b[a]=b[a]||c})(window.console=window.console||{});

	/**
	 * Logger, log to application
	 *
	 * @param label, string to identify log
	 * @param message, string to identify message
	 * @param callback, function to execute on storage
	 */
	window.log_this = function(label, message, callback) {
		var site_url = $('html').data('base_url');
		$.post(
			site_url + 'utils/log',
			{
				label: label,
				message: message
			},
			function(data){
				if(data == 'OK') {
					if (typeof callback == 'function') {
						callback();
					}
				}
		});
	};

	/**
	 * Check for mobile user agent
	 */
	window.isMobile = {
		Android: function() {
			return navigator.userAgent.match(/Android/i) ? true : false;
		},
		BlackBerry: function() {
			return navigator.userAgent.match(/BlackBerry/i) ? true : false;
		},
		iOS: function() {
			return navigator.userAgent.match(/iPhone|iPad|iPod/i) ? true : false;
		},
		iPhone: function() {
			return navigator.userAgent.match(/iPhone|iPod/i) ? true : false;
		},
		Windows: function() {
			return navigator.userAgent.match(/IEMobile/i) ? true : false;
		},
		any: function() {
			return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Windows());
		}
	};

	/**
	 * Case insensitive selector
	 */
	(function($) {
		$.expr[":"].Contains = $.expr.createPseudo(function(arg) {
				return function(elem) {
						return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
				};
		});
	})(jQuery)

	/**
	 * Preload images
	 */
	window.preload = function(images, callback) {
		$.each(images, function(index, value) {
			$('<img />').attr('src', value).load(function () {
				if (index == (images.length - 1)) {
					if (typeof callback == 'function') callback();
				}
			});
		});
	}
});