navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;
window.RTCPeerConnection = window.RTCPeerConnection || window.mozRTCPeerConnection || window.webkitRTCPeerConnection;
window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate || window.webkitRTCIceCandidate;
window.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;

var gateway;

var readyToStart = false; // set to true when connected to wss server

var localVideo;
var remoteVideo;
var peerConnection;
var peerConnectionConfig = {
    'iceServers': [{
            "urls": [
                "turn:104.199.132.30:3478?transport=udp",
                "turn:104.199.132.30:3478?transport=tcp",
                "turn:104.199.132.30:3479?transport=udp",
                "turn:104.199.132.30:3479?transport=tcp"
            ],
        "username":"1436130456:420614133",
        "credential":"7mkqvTDOzLHEs7pbqVcvMECuvqs="
    }]
};

function pageReady() {

    var vidWidth = getParameterByName('w');
    var vidHeight = getParameterByName('h');
    if(vidWidth == "" || !vidWidth)
        vidWidth = 320;
    if(vidHeight == "" || !vidHeight)
        vidHeight = 240;

    //console.log(vidWidth);
    //console.log(vidHeight);

    // simple way to get roomID
    // TODO: match this with our business logic (could be routing rule etc.)
    var roomID = window.location.hash.substring(1);
    if(!roomID || roomID == "") {
        roomID = '_r'+(new Date().getTime());
        //console.log("Hey look, somehow you're here all alone... Get your friend to room '"+roomID+"' will you?", 'warning');
    } else {
        //console.log("Joining room '"+roomID+"'...", 'info');
    }
    gateway = window.location.hostname+":10001/"+roomID;

    localVideo = document.getElementById('localVideo');
    remoteVideo = document.getElementById('remoteVideo');

    var constraints = {
        audio: true,
        video: {
            mandatory: {
                maxWidth: parseInt(vidWidth),
                maxHeight: parseInt(vidHeight)
            }
        }
    };

    if(navigator.getUserMedia) {
        navigator.getUserMedia(constraints, getUserMediaSuccess, getUserMediaError);
    } else {
        alert('Your browser does not support getUserMedia API');
    }
}

function initConnection() {
    serverConnection = new WebSocket('ws://'+gateway);
    serverConnection.onmessage = gotMessageFromServer;
    serverConnection.onopen = function(e) {
        // now that's the connection is there, it's safe to begin the game
        if(document.getElementById("start").value == "1") { // make sure that only 1 player inits the game session
            //console.log('Start');
            start(true);
        }
    };
    serverConnection.onerror = function(e) {
        //console.log("Network error. Doesn't look like you can play the game now...", "danger");
    };
}

function getUserMediaSuccess(stream) {
    localStream = stream;

    // we don't need to see ourselves, but if we do, this is where it's done
    //localVideo.src = window.URL.createObjectURL(stream);

    initConnection();
}

function getUserMediaError(error) {
    //console.log(error);
}

function start(isCaller) {
    peerConnection = new RTCPeerConnection(peerConnectionConfig);
    peerConnection.onicecandidate = gotIceCandidate;
    peerConnection.onaddstream = gotRemoteStream;
    peerConnection.addStream(localStream);

    if(isCaller) {
        var constraints = {
            mandatory: {
                OfferToReceiveAudio: true,
                OfferToReceiveVideo: true
            }
        };
        peerConnection.createOffer(gotDescription, createOfferError, constraints);
    }
}

function gotDescription(description) {
    //console.log('got description');
    //console.log(description);
    peerConnection.setLocalDescription(description, function () {
        serverConnection.send(JSON.stringify({
            'room': peerConnection.room,
            'sdp': description
        }));
    }, function() {//console.log('set description error')
     });
}

function gotIceCandidate(event) {
    //console.log(event.candidate);
    if(event.candidate != null) {
        serverConnection.send(JSON.stringify({
            'room': peerConnection.room,
            'ice': event.candidate
        }));
    }
}

function gotRemoteStream(event) {
    //console.log("got remote stream");
    remoteVideo.src = window.URL.createObjectURL(event.stream);
}

function createOfferError(error) {
    //console.log(error);
}

function gotMessageFromServer(message) {
    if(!peerConnection) start(false);

    var signal = JSON.parse(message.data);
    if(signal.sdp) {

        var constraints = {
            mandatory: {
                OfferToReceiveAudio: true,
                OfferToReceiveVideo: true
            }
        };

        //console.log(signal.sdp);
        peerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp), function() {
            peerConnection.createAnswer(gotDescription, createAnswerError);
        });
    } else if(signal.ice) {
        ////console.log('adding ice candidate...');
        //console.log(signal.ice);
        peerConnection.addIceCandidate(new RTCIceCandidate(signal.ice));
    } else if(signal.room) {
        peerConnection.room = signal.room; // VERY IMPORTANT
        //console.log('Welcome to room '+signal.room);
    } else if(signal.msg) { // generic message
        if(signal.msg == 'peerJoined'){}
            //console.log('Opponent spawned!!!');
    }
}

function createAnswerError(err) {
	//console.log(err);
}

// Helper functions
function endCall() {
  var videos = document.getElementsByTagName("video");
  for (var i = 0; i < videos.length; i++) {
    videos[i].pause();
  }

  peerConnection.close();
}

function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}