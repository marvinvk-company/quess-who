require.config({

	paths: {
		jquery: ['//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min', 'vendor/jquery-1.11.1'],
		backbone: 'vendor/backbone',
		underscore: 'vendor/underscore',
		'text': 'vendor/text',
		pusher: ['//js.pusher.com/2.2/pusher.min', 'vendor/pusher.min'],
		tweenMax: ['//cdnjs.cloudflare.com/ajax/libs/gsap/1.15.0/TweenMax.min', 'vendor/TweenMax.min'],
		webrtc: 'vendor/webrtc',
		facebookSDK: '//connect.facebook.net/en_US/all'
	},

	shim: {
		facebookSDK: {
			exports: 'FB'
		},
		'underscore': {
			exports: '_'
		},
		pusher: {
			deps: ['jquery'],
			exports: 'Pusher'
		}
	}

});

require(['views/app', 'lib/fb-helper', 'tweenMax'], function(MainView, FbHelper) {

	// set globals
	var appId = $('meta[name="app-id"]').attr('content');

	// App init
	window.App = {
		Vent: _.extend({}, Backbone.Events),
		Settings: {
			appId: appId,
			winWidth: $(window).width(),
			winHeight: $(window).height(),
			scrollTop: $(window).scrollTop(),
			laptopWidth: 1424,
			tabletWidth: 1024,
			smallTabletWidth: 767,
			mobileWidth: 599
		}
	};

	// initialize Facebook Helper
	window.fbHelper = new FbHelper({
		appId: App.Settings.appId
	});

	var startApp = function() {
		// start app
		var mainView = new MainView();

		$('*[data-controller]').each(function() {
			mainView.use($(this).data('controller'), $(this));
		});
	};

	startApp();

});
