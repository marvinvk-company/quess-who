---
- name: add nginx PPA
  sudo: true
  apt_repository: repo=ppa:nginx/stable
  register: nginx_apt

- name: Update apt cache
  apt: update_cache=yes
  when: nginx_apt.changed

- name: Install nginx
# When we use Ansible > 1.8 we can use a wildcard version of nginx as put in nginx_version.
#  apt: pkg="nginx={{ nginx_version }}" state=present
  apt: pkg="nginx" state=present
  notify:
    - restart nginx

- name: Add common web system dependencies
  apt: pkg={{ item }} state=present
  with_items:
    - git
    - libmysqlclient-dev
    - mysql-client
    - libpq-dev
    - gettext
    - supervisor

- name: Ensure nginx mime types
  # Added so they can be used together with gzip
  lineinfile: line="{{ item }}" insertbefore="}" dest=/etc/nginx/mime.types
  with_items:
    - application/x-font-ttf             ttf;
    - font/opentype                      ott;
  notify:
    - reload nginx

- name: Disable sendfile caching because Virtualbox is bugged.
  lineinfile:
    dest=/etc/nginx/nginx.conf
    backrefs=yes
    regexp=sendfile\s+on;
    line="sendfile off";
  when: deploy_environment == "local"
  notify:
    - reload nginx

- name: Copy nginx configuration snippets
  copy: dest="/etc/nginx/conf.d/{{ item }}" src="nginx/{{ item }}" force=yes
  with_items:
    - gzip.conf
    - server_names_hash_bucket_size.conf
    - client_max_body_size.conf
  notify:
    - reload nginx

- name: ensure supervisor is running and enabled
  service: name=supervisor state=running enabled=yes

- name: Set supervisor socket accessible to everyone
  ini_file: dest=/etc/supervisor/supervisord.conf section=unix_http_server
            option=chmod value=0777
  register: supervisor_config

- name: Restart supervisor
  service: name=supervisor state=restarted
  when: supervisor_config.changed

- name: Install python 2.7
  apt: pkg="{{ item }}" state=present
  with_items:
    - python2.7
    - python2.7-dev
    - python-pip
    - ruby-sass
    - ruby-compass

- name: Upgrade pip
  pip: name="pip" version="{{ pip_version }}" state=present

# Same version across servers
- name: Install Python virtualenv
  pip: name="{{ item.name }}" version="{{ item.version }}" state=present
  with_items:
    - { name: 'virtualenv', version: "{{ virtualenv_version }}" }
    - { name: 'virtualenvwrapper', version: "{{ virtualenvwrapper_version }}" }

- name: Add common web image dependencies
  apt: pkg={{ item }} state=present
  with_items:
    - libjpeg-dev
    - zlib1g-dev
    - libfreetype6
    - libfreetype6-dev
    - liblcms1-dev
    - libwebp-dev
    - imagemagick

- name: Add libtiff for 12.04
  apt: pkg={{ item }} state=present
  with_items:
    - libtiff4
    - libtiff4-dev
  when: ansible_lsb.release == '12.04'

- name: Add libtiff for 14.04
  apt: pkg={{ item }} state=present
  with_items:
    - libtiff5
    - libtiff5-dev
  when: ansible_lsb.release == '14.04'

- name: Install lessc (the less css compiler)
  npm:
    name=less
    version="{{ lessc_version }}"
    global=yes
    state=present
    production=yes

- shell: gem install compass

- name: Install bower
  npm:
    name=bower
    version="{{ bower_version }}"
    global=yes
    state=present
    production=yes

- name: Install grunt-cli
  npm:
    name=grunt-cli
    version="{{ grunt_cli_version }}"
    global=yes
    state=present
    production=yes

- name: Install PhantomJS
  npm:
    name=phantomjs
    version="{{ phantomjs_version }}"
    global=yes
    state=present
    production=yes

- name: Install Python MySQLDB for Ansible
  apt: pkg=python-mysqldb state=present

- name: Copy iptables rules
  copy: >
    src="{{ item }}"
    dest={{ iptables_rules_d }}/
  with_fileglob: iptables-rules.d/*.conf
  notify: restore iptables
